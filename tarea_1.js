//1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json
//2023-10-08T03:27:52.366+0000	connected to: mongodb://localhost/
//2023-10-08T03:27:55.042+0000	800 document(s) imported successfully. 0 document(s) failed to import.

//2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count? 
 db.grades.count()
//800


//3) Encuentra todas las calificaciones del estudiante con el id numero 4.
 db.grades.find({student_id:4},{_id:0,score:1})
//[
//    { score: 87.89071881934647 },
//    { score: 27.29006335059361 },
//    { score: 5.244452510818443 },
//    { score: 28.656451042441 }
//  ]

//4) ¿Cuántos registros hay de tipo exam?
db.grades.find({type:"exam"}).count() 
//200

//5) ¿Cuántos registros hay de tipo homework? 
db.grades.find({type:"homework"}).count() 
//400

//6) ¿Cuántos registros hay de tipo quiz? 
db.grades.find({type:"quiz"}).count() 
//200

//7) Elimina todas las calificaciones del estudiante con el id numero 3 
db.grades.updateMany({"student_id": 3}, {$unset:{"score": ""}})
/*
{
    acknowledged: true,
    insertedId: null,
    matchedCount: 4,
    modifiedCount: 4,
    upsertedCount: 0
  }
  students> db.grades.find({student_id:3})
  [
    {
      _id: ObjectId("50906d7fa3c412bb040eb585"),
      student_id: 3,
      type: 'homework'
    },
    {
      _id: ObjectId("50906d7fa3c412bb040eb584"),
      student_id: 3,
      type: 'quiz'
    },
    {
      _id: ObjectId("50906d7fa3c412bb040eb583"),
      student_id: 3,
      type: 'exam'
    },
    {
      _id: ObjectId("50906d7fa3c412bb040eb586"),
      student_id: 3,
      type: 'homework'
    }
  ]
*/

//8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ? 
db.grades.find({score : 75.29561445722392}).count()
//1

//9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

db.grades.update({_id:ObjectId("50906d7fa3c412bb040eb591")},{$set:{"score":100}})
/*
DeprecationWarning: Collection.update() is deprecated. Use updateOne, updateMany, or bulkWrite.
{
  acknowledged: true,
  insertedId: null,
  matchedCount: 1,
  modifiedCount: 1,
  upsertedCount: 0
}
*/

//10) A qué estudiante pertenece esta calificación.

db.grades.find({score:100})
/*

[
  {
    _id: ObjectId("50906d7fa3c412bb040eb591"),
    student_id: 6,
    type: 'homework',
    score: 100
  }
]
*/



